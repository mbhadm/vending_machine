/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vending_machine;
import java.util.*;
import vending_machine.model.Item;
import vending_machine.controllers.MoneyController;
import vending_machine.controllers.ItemController;
import vending_machine.model.Drink;
import vending_machine.model.Snack;

/**
 *
 * @author michael
 */
public class Vending_machine {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        
        boolean runProgram = true;
        Double moneyState = null ;
        MoneyController validateMoney = new MoneyController();

        while(runProgram) {
            
            boolean validationOfMoneyInProgress = true;
            
            System.out.println("Coins you can pay with (20, 10, 5, 2, 1) ");

            // Calling the vending machine controller
            ItemController controlItems = new ItemController();

            // Auto generating our vendin machine items
            controlItems.AutoGenerateItems();
            
            // Getting items from our vending machine controller
            ArrayList<Item> vendingMachineItems = controlItems.GetItemsArray();

            // Looping through arraylist with items returned from the controller
            for(int i = 0; i < vendingMachineItems.size(); i++ ) {             
                
                // Typecasting our item to subclass so we can get subclass methods
                if(vendingMachineItems.get(i) instanceof Drink) {
                    Drink thisDrink = (Drink) vendingMachineItems.get(i);
                    System.out.println("Product number:("+ i + ") " + thisDrink.GetName() + " - " + thisDrink.GetPrice() + ",-" + " - " + thisDrink.GetMililiter() + " ml");
                }
                
                if(vendingMachineItems.get(i) instanceof Snack) {
                    Snack thisSnack = (Snack) vendingMachineItems.get(i);
                    System.out.println("Product number:("+ i + ") " + thisSnack.GetName() + " - " + thisSnack.GetPrice() + ",-" + " - " + thisSnack.GetGram()+ " g");
                }
            }

            // Giving the user a choice
            System.out.println("Choose product to buy");
                        
            // Reading user input
            Scanner readUserInput = new Scanner(System.in);

            //Storing user input (user choice)
            int userInput = readUserInput.nextInt();

            if(vendingMachineItems.size() - 1 < userInput) {
                System.out.println("We dont have a product on that place: " + userInput);
            }
            else {
                //Print the users choice out
                System.out.println("Your choice: " + vendingMachineItems.get(userInput).GetName());

                // Asking the customer for money
                System.out.println("Plz pay " + vendingMachineItems.get(userInput).GetPrice());

                while(validationOfMoneyInProgress) {
                    // Reading the money from user insert
                    Scanner readInsertedMoney = new Scanner(System.in);
                    
                    // Storing the money input for validation
                    Double insertedMoney = readInsertedMoney.nextDouble();

                    // Make sure the customer has inserted acceptable coins
                    if(insertedMoney == 20.00 || insertedMoney == 10.00 || insertedMoney == 5.00 || insertedMoney == 2.00 || insertedMoney == 1.00) {
                        if(moneyState != null) {
                            moneyState = insertedMoney + moneyState;
                        }
                        else {
                            moneyState = insertedMoney;
                        }

                        
                        validateMoney.SetInsertedMoney(moneyState);
                        validateMoney.SetItemCost(vendingMachineItems.get(userInput).GetPrice());
                        Double validationResult = validateMoney.ValidateInsertedMoney();
                        
                        if(validationResult == 0) {
                            System.out.println("Thx ! enjoy your shit :) ");
                            validationOfMoneyInProgress = false;
                            moneyState = null;
                        }
                        else if(validationResult < 0) {
                            System.out.println("We need more money! " +validationResult);
                        }
                        else {
                            System.out.println("Thx ! enjoy your shit :) - Money returned " + validationResult);
                            validationOfMoneyInProgress = false;
                            moneyState = null;
                        }
                    }
                    else {
                        System.out.println("Use acceptable coins plz!");
                    }              
                }
            }
        }
    }
}
