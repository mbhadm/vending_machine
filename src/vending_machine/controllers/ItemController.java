/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vending_machine.controllers;

import java.util.*;
import vending_machine.model.Item;
import vending_machine.model.Drink;
import vending_machine.model.Snack;

/**
 *
 * @author michael
 */
public class ItemController {
    
    /** 
    * @param ItemsArray Holds the items for our vending machine 
    */
    private ArrayList<Item> ItemsArray = new ArrayList<>();
        
    public void AutoGenerateItems() {
        
        Drink item1 = new Drink();
        item1.SetName("Cola");
        item1.SetPrice(10.00);
        item1.SetMililiter(500);
        
        Drink item2 = new Drink();
        item2.SetName("Fanta");
        item2.SetPrice(12.00);
        item2.SetMililiter(500);
        
        Drink item3 = new Drink();
        item3.SetName("Sprite");
        item3.SetPrice(15.00);
        item3.SetMililiter(500);
        
        Snack item4 = new Snack();
        item4.SetName("Sourcreme and onion chips");
        item4.SetPrice(30.00);
        item4.SetGram(250);
        
        ItemsArray.add(item1);
        ItemsArray.add(item2);
        ItemsArray.add(item3);
        ItemsArray.add(item4);
    }
    
    public ArrayList GetItemsArray() {
        return this.ItemsArray;
    }
}
