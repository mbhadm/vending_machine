package vending_machine.controllers;
import java.util.*;
import vending_machine.model.Coin;
/**
 * @author michael
 */
public class MoneyController {
    /**
    * @param insertedMoney increments every time a customer puts more money in the vending machine
    * @param itemCost decreases everytime the customer puts more money in the vending machine
    */
    private Double insertedMoney;
    private Double itemCost;
    private ArrayList<Coin> coinsList;
    
    public void SetInsertedMoney(Double insertedMoney) {
        this.insertedMoney = insertedMoney;
    }
    
    public void SetItemCost(Double itemCost) {
        this.itemCost = itemCost;
    }
    
    public Double GetItemCost() {
        return this.itemCost;
    }
    
    public Double ValidateInsertedMoney() {
        
        // Validate if we should give money back to the customer og he/she hasnt payed enough
        if(this.insertedMoney > this.itemCost || this.insertedMoney < this.itemCost) {
            Double difference = this.insertedMoney - this.itemCost;
            return difference;
        } 
        else {
            // If the customer has payed enough, then we return 0 to represent the amount mathes the cost of the item.
            return 0.0;
        }
    }
    
    public ArrayList GenerateCoinsList() {
        Coin coin1 = new Coin();
        coin1.SetValue(20.00);
        
        Coin coin2 = new Coin();
        coin2.SetValue(10.00);
        
        Coin coin3 = new Coin();
        coin3.SetValue(5.00);
        
        Coin coin4 = new Coin();
        coin4.SetValue(2.00);
        
        Coin coin5 = new Coin();
        coin5.SetValue(1.00);
        
        coinsList.add(coin1);
        coinsList.add(coin2);
        coinsList.add(coin3);
        coinsList.add(coin4);
        coinsList.add(coin5);
        
        return coinsList;
    }
}

