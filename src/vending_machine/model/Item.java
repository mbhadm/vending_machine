/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vending_machine.model;

/**
 *
 * @author michael
 */
public class Item {
    
    private String name; 
    private Double price;
    
    public void SetName(String name) {
        this.name = name;
    }
    
    public void SetPrice(Double price) {
        this.price = price;
    }
    
    public String GetName() {
        return this.name;
    }
    
    public Double GetPrice() {
        return this.price;
    }
}
