/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vending_machine.model;

/**
 *
 * @author michael
 */
public class Snack extends Item{
        private int gram;
        
        public void SetGram(int gram) {
            this.gram = gram;
        }
        
        public int GetGram() {
            return this.gram;
        }
}
